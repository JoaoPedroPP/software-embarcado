/*
 * bsp.h
 *
 *  Created on: 14 de mar de 2017
 *      Author: tamandua32
 */

#ifndef BSP_H_
#define BSP_H_

#define BSP_TICKS_PER_SEC   1000U


void bsp_init(int t);
void bsp_on();
void bsp_off();
void tickHandler(void);

#endif /* BSP_H_ */
