#include "qpc.h"
#include "pisca.h"
#include "bsp.h"

/* Local-scope objects -----------------------------------------------------*/
static QEvt const *l_piscaQueueSto[QUEUESIZE];
static QSubscrList l_subscrSto[MAX_PUB_SIG];

/* storage for event pools... */
static QF_MPOOL_EL(PiscaEvt) l_smlPoolSto[POOLSIZE];         /* small pool */

/*..........................................................................*/
int main(int argc, char *argv[]) {

    Pisca_ctor();                    /* instantiate the Pisca active object */

    QF_init();     /* initialize the framework and the underlying RT kernel */

    bsp_init(0);           /* initialize the Board Support Package */
                                                  
    QF_psInit(l_subscrSto, Q_DIM(l_subscrSto));   /* init publish-subscribe */

                                               /* initialize event pools... */
    QF_poolInit(l_smlPoolSto, sizeof(l_smlPoolSto), sizeof(l_smlPoolSto[0]));


    QACTIVE_START(AO_Pisca, (uint8_t)(1),
                  l_piscaQueueSto, Q_DIM(l_piscaQueueSto),
                  (void *)0, 1024, (QEvt *)0);               /* 1K of stack */

    return QF_run();                              /* run the QF application */
}
