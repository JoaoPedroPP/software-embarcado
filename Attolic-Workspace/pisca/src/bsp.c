/*
 * bsp.c
 *
 *  Created on: 14 de mar de 2017
 *      Author: tamandua32
 */


#include "qpc.h"
#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <pthread.h>
#include "bsp.h"
#include "pisca.h"

pthread_t tudpServer;

struct sockaddr_in si_other;
int s;


char *out_signals[] = {
		"ledoff",
		"ledvermelho"
};

char *in_signals[] = {
		"B1",
		"B2"
};

void sendUDP(int sig){

	int slen = sizeof(si_other);
	int siglen;


	if(s != -1){

		siglen = strlen(out_signals[sig]);
		sendto(s, out_signals[sig], siglen, 0, (struct sockaddr*) &si_other, slen);
	}
}

void * udpServer()
{

	struct sockaddr_in si_other;
	struct sockaddr_in si_me;
	int i;
	PiscaEvt *pe;
    unsigned int slen = sizeof(si_other) , recv_len;
    char buf[BUFLEN];
	int ss = -1;

	ss = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if(s != -1){
        // zero out the structure
    	memset((char *) &si_me, 0, sizeof(si_me));
        si_me.sin_family = AF_INET;
        si_me.sin_port = htons(PORTIN);
        si_me.sin_addr.s_addr = htonl(INADDR_ANY);

	    //
		if (bind(ss, &si_me, sizeof(si_me))!=-1)
	    {
			buf[recv_len]='\0';

        while(1){

            if ((recv_len = recvfrom(ss, buf, BUFLEN, 0, (struct sockaddr *) &si_other, &slen)) != -1)
            {

            	for(i = 0; i < 2; i++)
            	{
            		if(strcmp(buf, in_signals[i])==0)
            		{

            			if(i == 0)
            			{
            				pe = Q_NEW(PiscaEvt, B1_SIG);
            				QF_PUBLISH(&pe->super, NULL);
            			}

            			if(i == 1)
            			{
            				pe = Q_NEW(PiscaEvt, B2_SIG);
            				QACTIVE_POST(AO_Pisca, &pe->super, NULL);
            			}
            		}
            	}
            }
        }}}
	    return NULL;
}

void bsp_on(){
	printf("on\n");
	sendUDP(1);

}

void bsp_off(){
	printf("off\n");
	sendUDP(0);
}

void bsp_init(int t){
			s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		    // zero out the structure
				memset((char *) &si_other, 0, sizeof(si_other));
				si_other.sin_family = AF_INET;
				si_other.sin_port = htons(PORTOUT);
				si_other.sin_addr.s_addr = htonl(INADDR_ANY);
			pthread_create(&tudpServer,NULL,udpServer,NULL);
}

/*..........................................................................*/
void QF_onStartup(void) {                               /* startup callback */
    QF_setTickRate(BSP_TICKS_PER_SEC,50);         /* set the desired tick rate */
}
/*..........................................................................*/
void QF_onCleanup(void) {                               /* cleanup callback */
    printf("\nBye! Bye!\n");
}
/*..........................................................................*/
void QF_onClockTick(void) {
    QF_TICK(&l_clock_tick);         /* perform the QF clock tick processing */
}
/*..........................................................................*/
void Q_onAssert(char const Q_ROM * const Q_ROM_VAR file, int line) {
    printf("Assertion failed in %s, line %d", file, line);
    QF_stop();
}
