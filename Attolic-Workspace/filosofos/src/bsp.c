/*
 * bsp.c
 *
 *  Created on: 14 de mar de 2017
 *      Author: tamandua32
 */


#include "qpc.h"
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include "bsp.h"
#include "dpp.h"

char *pensando = "filo0Pensando\0";
char *comendo = "filo0Comendo \0";
char *comfome = "filo0Com Fome\0";


static uint32_t l_rnd;
struct sockaddr_in si_other;
int s;

void sendUDP(int sig, int philo){
	char *signal;
	char buf[BUFLEN];
	char philoID;

	int slen = sizeof(si_other);
	int siglen;

	//printf("Filo  %d sig %d\n",philo, sig);

	if(s != -1){
    // zero out the structure
		switch(sig)
		{
			case 2:
				signal = comfome;
				break;
			case 1:
				signal = comendo;
				break;
			default:
				signal = pensando;
				break;
		}
		strcpy(buf, signal);
		philoID = '0';
		philoID += philo;
		buf[4] = philoID;
		siglen = strlen(buf);
		sendto(s, buf, siglen, 0, (struct sockaddr*) &si_other, slen);
	}
}


/*..........................................................................*/
void QF_onStartup(void) {                               /* startup callback */
    QF_setTickRate(BSP_TICKS_PER_SEC,50);         /* set the desired tick rate */
}
/*..........................................................................*/
void QF_onCleanup(void) {                               /* cleanup callback */
    printf("\nBye! Bye!\n");
}
/*..........................................................................*/
void QF_onClockTick(void) {
    QF_TICK(&l_clock_tick);         /* perform the QF clock tick processing */
}
/*..........................................................................*/
void Q_onAssert(char const Q_ROM * const Q_ROM_VAR file, int line) {
    printf("Assertion failed in %s, line %d", file, line);
    QF_stop();
}

/*..........................................................................*/
uint32_t BSP_random(void) {  /* a very cheap pseudo-random-number generator */
    /* "Super-Duper" Linear Congruential Generator (LCG)
    * LCG(2^32, 3*7*11*13*23, 0, seed)
    */
    l_rnd = l_rnd * (3*7*11*13*23);
    return l_rnd >> 8;
}
/*..........................................................................*/
void BSP_randomSeed(uint32_t seed) {
    l_rnd = seed;
}

void bsp_init(){
	s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	memset((char *) &si_other, 0, sizeof(si_other));
	si_other.sin_family = AF_INET;
	si_other.sin_port = htons(PORTOUT);
	si_other.sin_addr.s_addr = htonl(INADDR_ANY);
	BSP_randomSeed(1);
}

