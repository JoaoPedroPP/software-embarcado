#include "qp_port.h"
#include "bsp.h"
#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
                                      /* for snprintf() */




pthread_t tudpServer;

struct sockaddr_in si_other;
int s;


char *out_signals[] = {
		"micro_luz_on\0",
		"micro_luz_off\0",
		"micro_on\0",
		"micro_off\0"
};

char *in_signals[] = {
		"MicroCancela\0",
		"MicroAbrePorta\0",
		"MicroFechaPorta\0",
		"MicroPlus\0"
};

int in_sig[] = { CANCEL_SIG, OPEN_SIG,  CLOSE_SIG,   PLUS1_SIG};

void sendUDP(int sig){

	int slen = sizeof(si_other);
	int siglen;


	if(s != -1){

		siglen = strlen(out_signals[sig]);
		sendto(s, out_signals[sig], siglen, 0, (struct sockaddr*) &si_other, slen);

		printf("Sinal Enviado %s\n", out_signals[sig]);
	}
}

void sendUDPSeg(int seg, int num){
	char c;
	int slen = sizeof(si_other);
	int siglen;
	char displaySig[6];
	displaySig[0] = 'S';
	displaySig[1] = 'E';
	displaySig[2] = 'G';
	c = '0' + seg;
	displaySig[3] = c;
	c = '0' + num;
	displaySig[4] = c;
	displaySig[5] = '\0';

	if(s != -1){
		siglen = sizeof(displaySig);
		sendto(s, displaySig, siglen, 0, (struct sockaddr*) &si_other, slen);

		printf("Sinal enviado %s\n", displaySig);
	}
}

void * udpServer()
{

	struct sockaddr_in si_other;
	struct sockaddr_in si_me;
	int i;
	QEvt *e;
    unsigned int slen = sizeof(si_other) , recv_len;
    char buf[BUFLEN];
	int ss = -1;

	ss = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if(s != -1){
        // zero out the structure
    	memset((char *) &si_me, 0, sizeof(si_me));
        si_me.sin_family = AF_INET;
        si_me.sin_port = htons(PORTIN);
        si_me.sin_addr.s_addr = htonl(INADDR_ANY);

	    //
		if (bind(ss, &si_me, sizeof(si_me))!=-1)
	    {


        while(1){

            if ((recv_len = recvfrom(ss, buf, BUFLEN, 0, (struct sockaddr *) &si_other, &slen)) != -1)
            {
            	buf[recv_len]='\0';

            	for(i = 0; i < 4; i++)
            	{
            		if(strcmp(buf, in_signals[i])==0)
            		{
            			e = Q_NEW(QEvt, in_sig[i]);
            			QF_PUBLISH(e, NULL);

            			printf("Sinal identificado %d ", i);
            		}
            	}
            	printf(" Sinal Recebido %s\n", buf);
            }
        }}}
	    return NULL;
}
/* local variables ---------------------------------------------------------*/

/*..........................................................................*/
void QF_onStartup(void) {
    QF_setTickRate(BSP_TICKS_PER_SEC);         /* set the desired tick rate */
}
/*..........................................................................*/
void QF_onCleanup(void) {
}
/*..........................................................................*/
void QF_onClockTick(void) {
    static QEvt const tickEvt = { TIME_TICK_SIG, 0U, 0U };
    QF_TICK(&l_clock_tick);         /* perform the QF clock tick processing */
    QF_PUBLISH(&tickEvt, &l_clock_tick);          /* publish the tick event */
}
/*..........................................................................*/
void Q_onAssert(char const Q_ROM * const Q_ROM_VAR file, int line) {
    char message[80];
    QF_stop();                                              /* stop ticking */
    snprintf(message, Q_DIM(message) - 1U,
                "Assertion failed in module %s line %d", file, line);
}
/*..........................................................................*/
void BSP_init(void) {
	s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    // zero out the structure
		memset((char *) &si_other, 0, sizeof(si_other));
		si_other.sin_family = AF_INET;
		si_other.sin_port = htons(PORTOUT);
		si_other.sin_addr.s_addr = htonl(INADDR_ANY);
	pthread_create(&tudpServer,NULL,udpServer,NULL);
	BSP_digito(0, 0);
	BSP_digito(1, 1);
	BSP_digito(2, 2);
	BSP_digito(3, 3);
}
/*..........................................................................*/
void BSP_terminate(int result) {
    QF_stop();                        /* stop the main QF applicatin thread */

}

void BSP_forno(int i){
	if(i==0)
		sendUDP(3);
	else
		sendUDP(2);
}

void BSP_luz(int i){
	if(i==0)
		sendUDP(1);
	else
		sendUDP(0);
}

void BSP_digito(int seg, int num){
	sendUDPSeg(seg, num);
}

