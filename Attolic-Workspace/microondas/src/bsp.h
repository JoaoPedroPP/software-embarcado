
#ifndef bsp_h
#define bsp_h

#include "sinais.h"

#define BSP_TICKS_PER_SEC    1U
#define BUFLEN  512  //Max length of buffer
#define PORTIN  8888   //The port on which to listen for incoming data
#define PORTOUT 8889

void BSP_init(void);
void BSP_drawBitmap(uint8_t const *bitmap);
void BSP_terminate(int result);

void BSP_forno(int i);
void BSP_luz(int i);
void BSP_digito(int seg, int num);


#endif                                                             /* bsp_h */
