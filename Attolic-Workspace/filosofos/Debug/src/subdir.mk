################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/bsp.c \
../src/main.c \
../src/philo.c \
../src/table.c 

OBJS += \
./src/bsp.o \
./src/main.o \
./src/philo.o \
./src/table.o 

C_DEPS += \
./src/bsp.d \
./src/main.d \
./src/philo.d \
./src/table.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	gcc -I/opt/qpc/include -I/opt/qpc/src -I/opt/qpc/ports/posix-qv -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"

