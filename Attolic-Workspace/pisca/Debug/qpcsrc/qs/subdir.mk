################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
/opt/qpc/src/qs/qs.c \
/opt/qpc/src/qs/qs_64bit.c \
/opt/qpc/src/qs/qs_fp.c \
/opt/qpc/src/qs/qs_rx.c \
/opt/qpc/src/qs/qutest.c 

OBJS += \
./qpcsrc/qs/qs.o \
./qpcsrc/qs/qs_64bit.o \
./qpcsrc/qs/qs_fp.o \
./qpcsrc/qs/qs_rx.o \
./qpcsrc/qs/qutest.o 

C_DEPS += \
./qpcsrc/qs/qs.d \
./qpcsrc/qs/qs_64bit.d \
./qpcsrc/qs/qs_fp.d \
./qpcsrc/qs/qs_rx.d \
./qpcsrc/qs/qutest.d 


# Each subdirectory must supply rules for building sources it contributes
qpcsrc/qs/qs.o: /opt/qpc/src/qs/qs.c
	gcc -I/opt/qpc/include -I/opt/qpc/src -I/opt/qpc/ports/posix-qv -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
qpcsrc/qs/qs_64bit.o: /opt/qpc/src/qs/qs_64bit.c
	gcc -I/opt/qpc/include -I/opt/qpc/src -I/opt/qpc/ports/posix-qv -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
qpcsrc/qs/qs_fp.o: /opt/qpc/src/qs/qs_fp.c
	gcc -I/opt/qpc/include -I/opt/qpc/src -I/opt/qpc/ports/posix-qv -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
qpcsrc/qs/qs_rx.o: /opt/qpc/src/qs/qs_rx.c
	gcc -I/opt/qpc/include -I/opt/qpc/src -I/opt/qpc/ports/posix-qv -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
qpcsrc/qs/qutest.o: /opt/qpc/src/qs/qutest.c
	gcc -I/opt/qpc/include -I/opt/qpc/src -I/opt/qpc/ports/posix-qv -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"

