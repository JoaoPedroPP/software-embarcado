################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
/opt/qpc/src/qxk/qxk.c \
/opt/qpc/src/qxk/qxk_mutex.c \
/opt/qpc/src/qxk/qxk_sema.c \
/opt/qpc/src/qxk/qxk_xthr.c 

OBJS += \
./qpcsrc/qxk/qxk.o \
./qpcsrc/qxk/qxk_mutex.o \
./qpcsrc/qxk/qxk_sema.o \
./qpcsrc/qxk/qxk_xthr.o 

C_DEPS += \
./qpcsrc/qxk/qxk.d \
./qpcsrc/qxk/qxk_mutex.d \
./qpcsrc/qxk/qxk_sema.d \
./qpcsrc/qxk/qxk_xthr.d 


# Each subdirectory must supply rules for building sources it contributes
qpcsrc/qxk/qxk.o: /opt/qpc/src/qxk/qxk.c
	gcc -I/opt/qpc/include -I/opt/qpc/src -I/opt/qpc/ports/posix-qv -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
qpcsrc/qxk/qxk_mutex.o: /opt/qpc/src/qxk/qxk_mutex.c
	gcc -I/opt/qpc/include -I/opt/qpc/src -I/opt/qpc/ports/posix-qv -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
qpcsrc/qxk/qxk_sema.o: /opt/qpc/src/qxk/qxk_sema.c
	gcc -I/opt/qpc/include -I/opt/qpc/src -I/opt/qpc/ports/posix-qv -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
qpcsrc/qxk/qxk_xthr.o: /opt/qpc/src/qxk/qxk_xthr.c
	gcc -I/opt/qpc/include -I/opt/qpc/src -I/opt/qpc/ports/posix-qv -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"

