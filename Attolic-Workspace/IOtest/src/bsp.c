/*
 * bsp.c
 *
 *  Created on: 14 de mar de 2017
 *      Author: tamandua32
 */


#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include "bsp.h"
#include <pthread.h>


pthread_t tudpServer;

struct sockaddr_in si_other;
int s;




void sendUDP(char *sig){

	int slen = sizeof(si_other);
	int siglen;


	if(s != -1){

		siglen = strlen(sig);
		sendto(s, sig, siglen, 0, (struct sockaddr*) &si_other, slen);
	}
}

void * udpServer()
{

	struct sockaddr_in si_other;
	struct sockaddr_in si_me;


    unsigned int slen = sizeof(si_other) , recv_len;
    char buf[BUFLEN];
	int ss = -1;

	ss = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if(s != -1){
        // zero out the structure
    	memset((char *) &si_me, 0, sizeof(si_me));
        si_me.sin_family = AF_INET;
        si_me.sin_port = htons(PORTIN);
        si_me.sin_addr.s_addr = htonl(INADDR_ANY);

	    //
		if (bind(ss, &si_me, sizeof(si_me))!=-1)
	    {


        while(1){

            if ((recv_len = recvfrom(ss, buf, BUFLEN, 0, (struct sockaddr *) &si_other, &slen)) != -1)
            {
            	buf[recv_len] = '\0';
            	printf("RECEBIDO: %s\n", buf);
            	fflush(stdout);
            }
        }}}
	    return NULL;
}

void bsp_init(int t){
			s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		    // zero out the structure
				memset((char *) &si_other, 0, sizeof(si_other));
				si_other.sin_family = AF_INET;
				si_other.sin_port = htons(PORTOUT);
				si_other.sin_addr.s_addr = htonl(INADDR_ANY);
			pthread_create(&tudpServer,NULL,udpServer,NULL);
}


