################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
/opt/qpc/src/qf/qep_hsm.c \
/opt/qpc/src/qf/qep_msm.c \
/opt/qpc/src/qf/qf_act.c \
/opt/qpc/src/qf/qf_actq.c \
/opt/qpc/src/qf/qf_defer.c \
/opt/qpc/src/qf/qf_dyn.c \
/opt/qpc/src/qf/qf_mem.c \
/opt/qpc/src/qf/qf_ps.c \
/opt/qpc/src/qf/qf_qact.c \
/opt/qpc/src/qf/qf_qeq.c \
/opt/qpc/src/qf/qf_qmact.c \
/opt/qpc/src/qf/qf_time.c 

OBJS += \
./qpcsrc/qf/qep_hsm.o \
./qpcsrc/qf/qep_msm.o \
./qpcsrc/qf/qf_act.o \
./qpcsrc/qf/qf_actq.o \
./qpcsrc/qf/qf_defer.o \
./qpcsrc/qf/qf_dyn.o \
./qpcsrc/qf/qf_mem.o \
./qpcsrc/qf/qf_ps.o \
./qpcsrc/qf/qf_qact.o \
./qpcsrc/qf/qf_qeq.o \
./qpcsrc/qf/qf_qmact.o \
./qpcsrc/qf/qf_time.o 

C_DEPS += \
./qpcsrc/qf/qep_hsm.d \
./qpcsrc/qf/qep_msm.d \
./qpcsrc/qf/qf_act.d \
./qpcsrc/qf/qf_actq.d \
./qpcsrc/qf/qf_defer.d \
./qpcsrc/qf/qf_dyn.d \
./qpcsrc/qf/qf_mem.d \
./qpcsrc/qf/qf_ps.d \
./qpcsrc/qf/qf_qact.d \
./qpcsrc/qf/qf_qeq.d \
./qpcsrc/qf/qf_qmact.d \
./qpcsrc/qf/qf_time.d 


# Each subdirectory must supply rules for building sources it contributes
qpcsrc/qf/qep_hsm.o: /opt/qpc/src/qf/qep_hsm.c
	gcc -I/opt/qpc/include -I/opt/qpc/src -I/opt/qpc/ports/posix-qv -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
qpcsrc/qf/qep_msm.o: /opt/qpc/src/qf/qep_msm.c
	gcc -I/opt/qpc/include -I/opt/qpc/src -I/opt/qpc/ports/posix-qv -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
qpcsrc/qf/qf_act.o: /opt/qpc/src/qf/qf_act.c
	gcc -I/opt/qpc/include -I/opt/qpc/src -I/opt/qpc/ports/posix-qv -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
qpcsrc/qf/qf_actq.o: /opt/qpc/src/qf/qf_actq.c
	gcc -I/opt/qpc/include -I/opt/qpc/src -I/opt/qpc/ports/posix-qv -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
qpcsrc/qf/qf_defer.o: /opt/qpc/src/qf/qf_defer.c
	gcc -I/opt/qpc/include -I/opt/qpc/src -I/opt/qpc/ports/posix-qv -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
qpcsrc/qf/qf_dyn.o: /opt/qpc/src/qf/qf_dyn.c
	gcc -I/opt/qpc/include -I/opt/qpc/src -I/opt/qpc/ports/posix-qv -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
qpcsrc/qf/qf_mem.o: /opt/qpc/src/qf/qf_mem.c
	gcc -I/opt/qpc/include -I/opt/qpc/src -I/opt/qpc/ports/posix-qv -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
qpcsrc/qf/qf_ps.o: /opt/qpc/src/qf/qf_ps.c
	gcc -I/opt/qpc/include -I/opt/qpc/src -I/opt/qpc/ports/posix-qv -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
qpcsrc/qf/qf_qact.o: /opt/qpc/src/qf/qf_qact.c
	gcc -I/opt/qpc/include -I/opt/qpc/src -I/opt/qpc/ports/posix-qv -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
qpcsrc/qf/qf_qeq.o: /opt/qpc/src/qf/qf_qeq.c
	gcc -I/opt/qpc/include -I/opt/qpc/src -I/opt/qpc/ports/posix-qv -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
qpcsrc/qf/qf_qmact.o: /opt/qpc/src/qf/qf_qmact.c
	gcc -I/opt/qpc/include -I/opt/qpc/src -I/opt/qpc/ports/posix-qv -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
qpcsrc/qf/qf_time.o: /opt/qpc/src/qf/qf_time.c
	gcc -I/opt/qpc/include -I/opt/qpc/src -I/opt/qpc/ports/posix-qv -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"

