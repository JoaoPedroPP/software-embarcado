/*
 * micro.c
 *
 */
#include "bsp.h"

#include "qp_port.h"
#include "bsp.h"
#include "qassert.h"

/*..........................................................................*/
typedef struct TMicroTag {             /* Transaction Server active object */
    QActive super;                                   /* derive from QActive */
} TMicro;

static TMicro l_TMicro;                      /* the TMicro active object */
QActive * const AO_tmicro = (QActive *)&l_TMicro;
/* Microwave control ctor */
void TMicro_ctor();

/* hierarchical state machine ... */
static QState TMicro_off       (TMicro * const me, QEvt const * const e);
static QState TMicro_on  (TMicro * const me, QEvt const * const e);
static QState TMicro_initial(TMicro * const me, QEvt const *e);

/*..........................................................................*/
void TMicro_ctor() {                 /* the default ctor */
	TMicro * const me = &l_TMicro;
	QActive_ctor(&me->super, Q_STATE_CAST(&TMicro_initial));
}

/* HSM definition ----------------------------------------------------------*/
QState TMicro_initial(TMicro * const me, QEvt const *e)
{
	BSP_forno(0);
	BSP_luz(0);

	QActive_subscribe((QActive *)me, TIME_TICK_SIG);
	QActive_subscribe((QActive *)me, PLUS1_SIG);
	QActive_subscribe((QActive *)me, OPEN_SIG);
	QActive_subscribe((QActive *)me, CLOSE_SIG);
	QActive_subscribe((QActive *)me, CANCEL_SIG);

	return Q_TRAN(&TMicro_off);
}
/*..........................................................................*/
QState TMicro_off(TMicro * const me, QEvt const * const e) {
    QState status;
    switch (e->sig) {
    	case OPEN_SIG: {
    		BSP_luz(1);
    		status = Q_HANDLED();
    		break;
    	}
    	case CLOSE_SIG: {
    		BSP_luz(0);
    		status = Q_HANDLED();
    		break;
    	}
    	case PLUS1_SIG: {
    		BSP_luz(1);
    		status = Q_TRAN(&TMicro_on);
    		break;
    	}
    	case CANCEL_SIG: {
    		status = Q_HANDLED();
    		break;
    	}
    	case TIME_TICK_SIG: {
    		status = Q_HANDLED();
    		break;
    	}
        default: {
            status = Q_SUPER(&QHsm_top);
            break;
        }
    }
    return status;
}
/*..........................................................................*/
QState TMicro_on(TMicro * const me, QEvt const * const e) {
    QState status;
    switch (e->sig) {
    	case OPEN_SIG: {
    		BSP_luz(1);
    		status = Q_HANDLED();
    		break;
    	}
    	case CLOSE_SIG: {
    		BSP_luz(1);
    		status = Q_HANDLED();
    		break;
    	}
    	case PLUS1_SIG: {
    		status = Q_HANDLED();
    		break;
    	}
    	case CANCEL_SIG: {
    		BSP_luz(0);
    		status = Q_TRAN(&TMicro_off);
    		break;
    	}
        default: {
            status = Q_SUPER(&QHsm_top);
            break;
        }
    }
    return status;
}




