/*
 * main.c
 *
 */
#include "bsp.h"


#include "qp_port.h"
#include "bsp.h"
#include "qassert.h"

static QEvt const *l_TMicroQSto[10];    /* Event queue storage for TMicro */
static QEvt   l_smlPoolSto[10];  /* small-size pool */
static QSubscrList    l_subscrSto[MAX_PUB_SIG];        /* publish-subscribe */


/*..........................................................................*/
int main(int argc, char *argv[]) {

	TMicro_ctor();

    QF_init();     /* initialize the framework and the underlying RT kernel */
    BSP_init();
                                               /* initialize the event pools... */
    QF_poolInit(l_smlPoolSto, sizeof(l_smlPoolSto), sizeof(l_smlPoolSto[0]));


    QF_psInit(l_subscrSto, Q_DIM(l_subscrSto));   /* init publish-subscribe */
                 /* no initialization event */
    QActive_start(AO_tmicro,      /* global pointer to the Ship active object */
                      1,                                            /* priority */
                      l_TMicroQSto,    Q_DIM(l_TMicroQSto),    /* evt queue */
                      (void *)0, 0,                      /* no per-thread stack */
                      (QEvt *)0);                  /* no initialization event */


    QF_run();                                     /* run the QF application */
    return 0;
}

